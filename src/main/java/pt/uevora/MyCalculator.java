package pt.uevora;

import java.util.Scanner;

public class MyCalculator {

    public Double execute(String expression){
        

        if(expression.contains("+")){
            String[] split = expression.split("\\+");
            return Sum(split[0], split[1]);
        }
        if(expression.contains("-")){
            String[] split = expression.split("\\-");
            return Subtract(split[0], split[1]);
        }
        if(expression.contains("*")){
            String[] split = expression.split("\\*");
            return Multiply(split[0], split[1]);
        }
        if(expression.contains("/")){
            String[] split = expression.split("\\/");
            return Division(split[0], split[1]);
        }
        if(expression.contains("^")){
            String[] split = expression.split("\\^");
            return Power(split[0], split[1]);
        }


        throw new IllegalArgumentException("Invalid expression!");
    }

    private Double Sum(String arg1, String arg2) {
        return new Double(arg1) + new Double(arg2);
    }
    private Double Subtract(String arg1, String arg2){
        return new Double(arg1) - new Double(arg2);
    }
    private Double Multiply(String arg1, String arg2){
        return new Double(arg1) * new Double(arg2);
    }
    private Double Division(String arg1, String arg2){
        return new Double(arg1) / new Double(arg2);
    }
    private Double Power(String arg1, String arg2){
        return Math.pow(new Double(arg1), new Double(arg2));
    }

    public static void main(String[] args) {
        System.out.println("Calculator");
        System.out.println("Enter your expression:");
        Scanner scanner = new Scanner(System.in);
        String expression = scanner.nextLine();
        MyCalculator myCalculator = new MyCalculator();
        Object result = myCalculator.execute(expression);

        System.out.println("Result :  " + result);
        if(scanner != null){
            scanner.close();
        }
    }

}
