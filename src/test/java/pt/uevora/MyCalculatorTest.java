package pt.uevora;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class MyCalculatorTest {

    MyCalculator calculator;

    @Before
    public void setUp(){
        calculator = new MyCalculator();
    }

    @After
    public void down(){
        calculator = null;
    }

    @Test
    public void testSum() throws Exception {
        Double result = calculator.execute("2+3");
        assertEquals("The sum result of 2 + 3 must be 5",  5D, (Object)result);
    }

    @Test
    public void testSubtract() throws Exception {
        Double result = calculator.execute("5-4");
        assertEquals("The subtraction result of 5 - 4 must be 1", 1D, (Object)result);
    }

    @Test
    public void testMultiply() throws Exception {
        Double result = calculator.execute("4*3");
        assertEquals("The multiplication result of 4 * 3 must be 12", 12D,(Object)result);
    }

    @Test
    public void testDivision() throws Exception {
        Double result = calculator.execute("8/2");
        assertEquals("The division result of 8 / 2 must be 4", 4D, (Object)result);
    }

    @Test
    public void testPower() throws Exception {
        Double result = calculator.execute("3^2");
        assertEquals("The potency result of 3 ^ 2 must be 9", 9D, (Object)result);
    }
}